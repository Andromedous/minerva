# What is Minerva
According to legend, Minerva was the goddess of wisdom and creativity.  
According to astronomy, 93 Minerva is one of the first 100 minor planets discoverd in our solar system.  
According to us, Minerva is a CTF Building framework!

# A CTF Framework?
Minerva offers a simple way to build and host your own CTF challenges.  
Set it up, Log in and start adding challenges - Very simple!

# How is it built?
Minerva is a web app.  
The front-end is built using Angular.js and Bootstrap.  
The back-end is a REST-API server built using Sanic - a fork of Flask.  
The back-end uses a MongoDB based database to store data.
